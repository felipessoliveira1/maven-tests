package jbehave.spring.fluent;

import jbehave.spring.fluent.jbehaveSteps.GivenSteps;
import jbehave.spring.fluent.jbehaveSteps.ThenSteps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration
public class StepsContext {

    @Autowired
    public GivenSteps myStepsOne;
    @Autowired
    public ThenSteps myStepsTwo;
}
