package jbehave.spring.fluent.jbehaveSteps;

import jbehave.spring.fluent.assertions.StepsAssertions;
import jbehave.spring.fluent.jbehavePageObjects.MyStepsTwoPageObject;

import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Then;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ThenSteps {

    @Autowired
    private MyStepsTwoPageObject myStepsTwoPageObject;

    @Then("I should be on $url")
    public void givenIShouldBeOnHttplocalhostPage(String url) {
        myStepsTwoPageObject.clickCookiesButtonIfExists();
        String currentPage = myStepsTwoPageObject.getUrl();
        String subUrl = url.substring(8);
        assertTrue("Not found", currentPage.contains(subUrl));
    }
}
