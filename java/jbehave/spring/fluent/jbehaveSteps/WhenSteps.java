package jbehave.spring.fluent.jbehaveSteps;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.jbehave.core.annotations.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import freemarker.log.Logger;
import jbehave.spring.fluent.driverProvider.DriverProvider;
import jbehave.spring.fluent.jbehavePageObjects.MyStepsOnePageObject;

@Component
public class WhenSteps {

	@Autowired
	private WebDriver driver;

	@When("I login with $user and $password to $url")
	public void testAuthenticationFailureWhenProvidingBadCredentials(String user, String password, String url) {
		driver.findElement(By.cssSelector("div.usr-grt-text")).click();
		driver.findElement(By.id("h_usr-signin")).click();
		driver.findElement(By.id("email-input")).sendKeys(user);
		driver.findElement(By.id("password")).sendKeys(password);
		driver.findElement(By.id("login-button")).click();

		final List<WebElement> iframes = driver.findElements(By.tagName("iframe"));
		for (WebElement iframe : iframes) {
			System.out.println("Frame Title :" + iframe.getAttribute("title"));
			if(iframe.getAttribute("title").equals("desafio reCAPTCHA"))
			{
				assertNotNull(iframe.getAttribute("name"));
				break;
			}

		}
	}
}