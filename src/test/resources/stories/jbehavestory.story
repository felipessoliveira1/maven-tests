Narrative:
As a user 
I want to visit jbehave page and reuse steps from another story

Scenario: Visit o2 page
Given I go to <url>
When I login with <user> and <password> to <url>
Then I should be on <url>
Examples:
| url  							| user  		| password |
|https://www.americanas.com.br	| user1@url.com | pass1    |


Scenario: Visit the same o2 page as second scenario
Given I go to <url>
When I login with <user> and <password> to <url>
Then I should be on <url>
Examples:
| url  							| user  		| password |
|https://www.americanas.com.br	| user1@url.com | pass1    |
